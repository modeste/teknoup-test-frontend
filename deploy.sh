echo "Install Dependences..."
npm install
echo "Building AAVIE project for production..."
npm run build
echo "Copying built files..."
rm -rf tecknoup-prod/dist/
cp -R dist/ tecknoup-prod/dist/
cd tecknoup-prod
echo "Install Dependences..."
npm install
cd ..
pm2 restart all
echo "Restart Nginx...."
service nginx restart

