export const mixin = {
  methods: {
    getSubDomain(route) {
      //...
      if (route.length > 1) {
        return route[0]
      } else {
        return null
      }
    },
    getFileUploaded(url) {
      return process.env.VUE_APP_BACKEND_URL + "/downloads/" + url
    }
  },
}
