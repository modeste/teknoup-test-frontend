export default {
  frontUrl: process.env.VUE_APP_FRONT_URL,
  serverURL: process.env.VUE_APP_BACKEND_URL,
}
