import Vue from "vue";
import { FETCH_ALL_ANNONCE, ADD_ANNONCE, UPDATE_ANNONCE, REMOVE_ANNONCE } from "./mutation-types";

export default {
    [FETCH_ALL_ANNONCE](state, data) {
        state.annonces = data.reverse();
    },
    [ADD_ANNONCE](state, data) {
        let all = [...state.annonces]
        all.unshift(data)
        state.annonces = all
    },
    [UPDATE_ANNONCE](state, data) {
        const index = state.annonces.findIndex((el)=>el.id === data.id)
        Vue.set(state.annonces, index, data)
    },
    [REMOVE_ANNONCE](state, id) {
        const index = state.annonces.findIndex((el)=>el.id === id)
        state.annonces.splice(index, 1);
    },
}
