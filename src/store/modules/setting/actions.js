import { FETCH_ALL_ANNONCE, ADD_ANNONCE, UPDATE_ANNONCE, REMOVE_ANNONCE } from "./mutation-types";

import service from "@/service";
import config from "@/config";
import axios from "axios";

const $service = service(axios, config);

export default {
    async applyUploadFile({ commit }, data) {
        return await $service.uploadFile(data);
    },
    async applyGetAllAnnonce({ commit }, data) {
        const items = await $service.getAllAnnonce(data);
        commit(FETCH_ALL_ANNONCE, items);
    },
    async applyGetAnnonce({ commit }, data) {
        return await $service.getAnnonce(data);
    },
    async applyAddAnnonce({ commit }, data) {
        const item = await $service.addAnnonce(data);
        commit(ADD_ANNONCE, item);
    },
    async applyUpdateAnnonce({ commit }, data) {
        const item = await $service.updateAnnonce(data);
        commit(UPDATE_ANNONCE, item);
    },
    async applyRemoveAnnonce({ commit }, data) {
        let { annonceId } = data
        await $service.removeAnnonce(data)
        commit(REMOVE_ANNONCE, annonceId)
    },
}
