import Vue from "vue";
import App from "./App.vue";
import router from '@/router'

import store from '@/store'
import service from '@/service'
import config from '@/config'
import axios from 'axios'

import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
Vue.component('v-select', vSelect)

import Vuikit from 'vuikit'
import VuikitIcons from '@vuikit/icons'
import '@vuikit/theme'
Vue.use(Vuikit)
Vue.use(VuikitIcons)

import '@/assets/css/global.css'

// VeeValidate
import VeeValidate, { Validator } from "vee-validate";
import en from "vee-validate/dist/locale/en";
Validator.localize({ en: en });
Vue.use(VeeValidate, { locale: "en" });

// Globally Registered Components
import './globalComponents.js'

Vue.config.productionTip = false

Vue.prototype.$config = config
Vue.prototype.$service = service(axios, config)

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
