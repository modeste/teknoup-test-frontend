import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routes = [
  {
    path: "/",
    name: 'Home',
    component: () => import("@/views/Home"),
  },
  {
    path: "/annonce",
    name: 'ViewAnnonce',
    component: () => import("@/views/Annonce"),
  },

  // ...
]

const router = new Router({
  base: "/",
  mode: 'history',
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes,
})

Vue.router = router

export default router
