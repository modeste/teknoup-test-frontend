import Vue from 'vue'

Vue.component('teknoup-header', () => import('@/components/Header'))
Vue.component('teknoup-footer', () => import('@/components/Footer'))
