/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
import { AUTH_TOKEN_KEY } from "@/constants";
import { mixin } from "@/mixins/mixin";

const API_VERSION = "v1"
 
export default ($http, $config) => {
  const $api = $http.create({
    baseURL: $config.serverURL,
    headers: { "Content-Type": "application/json" }
  });

  $api.interceptors.request.use(
    config => { 
      return config;
    },
    error => {
      return Promise.reject(error);
    }
  )

  const uploadFile = data => {
    return $api.post('/api/' + API_VERSION + '/operation/upload-file', data).then(res => res.data);
  };

  const getAllAnnonce = data => {
    return $api.get('/api/' + API_VERSION + '/annonce', data)
      .then(res => res.data)
  }

  const getAnnonce = data => {
    return $api.get('/api/' + API_VERSION + '/annonce/' + data.id, data)
      .then(res => res.data)
  }

  const addAnnonce = data => {
    return $api.post('/api/' + API_VERSION + '/annonce', data)
      .then(res => res.data)
  }

  const updateAnnonce = data => {
    return $api.patch('/api/' + API_VERSION + '/annonce/' + data._id, data)
      .then(res => res.data)
  }

  const removeAnnonce = data => {
    return $api.post('/api/' + API_VERSION + '/operation/annonce/remove', data)
      .then(res => res.data)
  }

  return {
    uploadFile,
    getAllAnnonce, getAnnonce, addAnnonce, updateAnnonce, removeAnnonce,
  }
}
