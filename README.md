# teknoup-test-frontend
## Project setup

Intall Package 
### `npm install`

Compiles and minifies for production
### `npm run build`

Start application
### `npm run serve`

Start application and set port
### `npm run serve -- --port 4002`
